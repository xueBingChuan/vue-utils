<div align="center">
  <a href="https://gridea.dev">
    <img src="https://gitee.com/xueBingChuan/nodejs-webserve/raw/master/src/static/face.jpg"  width="80px" height="80px">
  </a>
  <h1 align="center">
     vue-utils
  </h1>
  <h3 align="center">
   仿照vue2,vue3的源码实现的功能拆分使用,及包含常用的工具函数(操作cookie,history路由实现等....)
  </h3>
</div>

## 插件使用
由于插件采用原生js编写且不依赖任何第三方库，因此它可以在任意一台支持js的设备上运行。
> 注意⚠️：如果想将该插件引入大型项目中可直接按照下方方式进行引入,且实例化插件才可以使用，否则插件无法正常工作

### import形式使用插件
*** 需要在引入后进行实例化 ***
```javascript
* npm install xue-vue2

* import vueUtils from "xue-vue2"

* new vueUtils
```

### cdn形式使用插件
* 因为使用nodejs-webserve项目将依赖中的插件进行读取并处理为一个接口
* 所以可以直接使用`script`标签类似cdn的方式引入到项目中
```javascript
<script src="http://bing-chuan.work/get_cdn/xue-vue2"></script>
```

* 在业务代码中使用时实例化插件即可(此处只展示双向绑定和watch,其他功能的支持接着往下看......)
```javascript
import vueUtils from "xue-vue2";
let vue = {
    //挂载的dom节点元素
    el: "#div1",
    //需要双向绑定(订阅发布)的数据
    data: {
        obj: {
            data: "调试",
        },
    },
    //需要进行监听的双向绑定数据
    wathc:{
        obj(val, oldVal) {
            console.log("watch", val, oldVal);
        }
    }
}
new vueUtils(vue)
```

# 目前支持功能

- [x] `虚拟DOM`
- [x] `Diff更新`
- [x] `{{ data }}` or `{{ data + 'test' }}` or `{{ fn(data) }}`
- [x] `v-for` // `v-for="(item, index) in list"` or `v-for="(item, index) in 10"` or `v-for="(item, index) in 'string'"`
- [x] `v-if` `v-else-if` `v-else`
- [x] `v-show`
- [x] `v-html`
- [x] `v-model`
- [x] `v-click` `@click` 点击事件
- [x] `computed` 计算属性
- [x] `watch` 监听
- [x] `beforeCreate`、`created`、`beforeMount`、`mounted`、`beforeUpdate`、`updated`
- [x] `:class`
- [x] `:style`
- [x] `$nextTick`
- [x] `ref`
- [x] `reactive`
- [x] `toRefs`
- [x] `watchEffect`
- [x] `Context`

  
# 补充

`npm发包`,`虚拟dom`,`双向绑定`,`diff算法`等 不懂的可以看看我之前发的文章(相关代码相比现在有部分改动)：http://bing-chuan.work/blog
